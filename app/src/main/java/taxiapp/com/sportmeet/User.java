package taxiapp.com.sportmeet;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("userID")
    private String id;
    @ SerializedName("userName")
    private String username;


    public User(String id, String username) {
        this.id = id;
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }


}
