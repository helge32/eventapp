package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment6 extends Fragment {

    private TextView title;
    private TextView category;
    private TextView location;
    public TextView selectedDate;
    private TextView timeSelectedView;
    private AddEventFrag aF;
    private EditText description;
    public AddEventFragment6(AddEventFrag aF){

        this.aF = aF;

    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout6, container, false);



        selectedDate = v.findViewById(R.id.whenTextView2);
        title = v.findViewById(R.id.titleTextView);
        category = v.findViewById(R.id.categoryTextView);
        location = v.findViewById(R.id.whereTextView);
        timeSelectedView = v.findViewById(R.id.whatTimeTextView2);
        description = v.findViewById(R.id.description);

        selectedDate.setText(aF.eventDetails.get("date"));
        title.setText(aF.eventDetails.get("name"));
        category.setText((aF.eventDetails.get("category")));
        location.setText(aF.eventDetails.get("location"));
        timeSelectedView.setText(aF.eventDetails.get("time"));



        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String desc = description.getText().toString();
                if(!desc.equals("")){

                    aF.eventDetails.put("description",desc);
                    aF.switchLayout(7);

                }



            }
        });


        return v;
    }


}