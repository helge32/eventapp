package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import java.lang.ref.WeakReference;

@SuppressLint("ValidFragment")
public class EventDialog extends DialogFragment {
    Context mContext;
private WeakReference<AddEventFragment5> weakReference;

public  EventDialog(AddEventFragment5 addEventFragment4){

    weakReference = new WeakReference<>(addEventFragment4);

}
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setTitle("Event Hinzufügen");
        alertDialogBuilder.setMessage("Bist du sicher, dass du das Event hinzufügen willst");
        //null should be your on click listener
        alertDialogBuilder.setPositiveButton("Hinzufügen", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              //  weakReference.get().registerEvent2();
            }
        });
        alertDialogBuilder.setNegativeButton("Nein", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {




                dialog.dismiss();
            }
        });


        return alertDialogBuilder.create();
    }
}