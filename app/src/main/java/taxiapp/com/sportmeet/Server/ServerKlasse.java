package taxiapp.com.sportmeet.Server;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import android.os.Handler;
import android.view.View;
import android.widget.ListView;


import com.google.android.gms.location.FusedLocationProviderClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.EventTiming;
import taxiapp.com.sportmeet.Fragments.MainFragment;
import taxiapp.com.sportmeet.Fragments.MyEventsFragment;
import taxiapp.com.sportmeet.MainActivity;

public class ServerKlasse extends AsyncTask<String, String, String> implements Runnable {
  public  int ident  =0;

String request ="";



    ListView lv;
    MainActivity mainActivity;
    WeakReference<MainFragment> mainFragmentWeakReference;
    WeakReference<MyEventsFragment> myEventsFragmentWeakReference;
   Context context;


   public ServerKlasse(Context context,MainFragment mainFragment,MyEventsFragment myEventsFragment, MainActivity mainActivity){
       this.mainActivity = mainActivity;
       mainFragmentWeakReference = new WeakReference<>(mainFragment);
       myEventsFragmentWeakReference = new WeakReference<>(myEventsFragment);
        this.context = context;


    }

    MainFragment mf;
   public double lat;
   public   double lon;
   private FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }


    @SuppressLint("MissingPermission")
    @Override
    protected String doInBackground(String... args) {



       String type = args[0];

       String eventAddURL = "https://eventmeetapp.com/start.php";
       if(type.equals("eventAdd")){
           try {
               ident = 2;
                request = "eventAdd";
               //Kategorie,
               String name = args[1];

               String wo = args[2];

               String was = args[3];
               String wann = args[4];

               String beschreibung = args[5];
              String lat = args[6];
               String lon = args[7];

               String maxTeilnehmer = args[8];




               URL url = new URL(eventAddURL);
               HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
               httpURLConnection.setRequestMethod("POST");
               httpURLConnection.setDoOutput(true);
               httpURLConnection.setDoInput(true);
               OutputStream outputStream = httpURLConnection.getOutputStream();

               BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
               String post_data = URLEncoder.encode("event_db_request_type","UTF-8")+"="+URLEncoder.encode("eventAdd","UTF-8")+"&"
                       +URLEncoder.encode("event_date","UTF-8")+"="+URLEncoder.encode(wann,"UTF-8")+"&"
                       +URLEncoder.encode("event_location","UTF-8")+"="+URLEncoder.encode(wo,"UTF-8") +"&"
                       +URLEncoder.encode("event_latitude","UTF-8")+"="+URLEncoder.encode(lat,"UTF-8") +"&"
                       +URLEncoder.encode("event_longitude","UTF-8")+"="+URLEncoder.encode(lon,"UTF-8") +"&"
                       +URLEncoder.encode("event_kategorie","UTF-8")+"="+URLEncoder.encode(was,"UTF-8")+"&"
                       +URLEncoder.encode("event_startTime","UTF-8")+"="+URLEncoder.encode("10:20:00","UTF-8")+"&"
                       +URLEncoder.encode("event_endTime","UTF-8")+"="+URLEncoder.encode("10:20:00","UTF-8")+"&"
                       +URLEncoder.encode("event_beschreibung","UTF-8")+"="+URLEncoder.encode(beschreibung,"UTF-8")+"&"
                       +URLEncoder.encode("event_maxTeilnehmer","UTF-8")+"="+URLEncoder.encode(maxTeilnehmer,"UTF-8")+"&"
                       +URLEncoder.encode("event_name","UTF-8")+"="+URLEncoder.encode(name,"UTF-8");



               bufferedWriter.write(post_data);
               System.out.println(post_data);
               bufferedWriter.flush();
               bufferedWriter.close();
               outputStream.close();
               InputStream inputStream = httpURLConnection.getInputStream();
               BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
               String result="";
               String line="";
               while((line = bufferedReader.readLine())!= null) {
                   result += line;

               }

               bufferedReader.close();
               inputStream.close();
               httpURLConnection.disconnect();
               return result;
           } catch (MalformedURLException e) {
               e.printStackTrace();
           } catch (IOException e) {
               e.printStackTrace();
           }
       }


       else if (type.equals("loadData")){

           ident = 1;

           try {

              double lat = Double.valueOf(args[1]);
              double lon = Double.valueOf(args[2]);

               URL url = new URL(eventAddURL);
               HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
               httpURLConnection.setRequestMethod("POST");
               httpURLConnection.setDoOutput(true);
               httpURLConnection.setDoInput(true);
               OutputStream outputStream = httpURLConnection.getOutputStream();
               BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
               String post_data = URLEncoder.encode("event_db_request_type","UTF-8")+"="+URLEncoder.encode("loadEvents","UTF-8")
              +"&"+URLEncoder.encode("user_key","UTF-8")+"="+URLEncoder.encode("12345","UTF-8");


               System.out.println(post_data);
               bufferedWriter.write(post_data);
               bufferedWriter.flush();
               bufferedWriter.close();
               outputStream.close();
               InputStream inputStream = httpURLConnection.getInputStream();
               BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
               String result="";
               String line="";
               while((line = bufferedReader.readLine())!= null) {
                   result += line;

               }


               System.out.println(result);
               mf = mainFragmentWeakReference.get();


           mf.events.clear();
               JSONObject mainObject = new JSONObject(result);
               JSONArray js = mainObject.getJSONArray("posts");


               System.out.println(result);
               for(int i = 0; i<js.length();i++){


                   JSONObject ja = js.getJSONObject(i);
                   String eventName = ja.getString("eventName");
                   String eventLocation = ja.getString("eventLocation");
                   double eventLatitude = ja.getDouble("latitude");
                   double eventLongitude = ja.getDouble("longitude");
                   String eventDate = ja.getString("eventDate");
                   String eventID = ja.getString("eventID");
                   String eventCategory = ja.getString("eventKategorie");
                   String eventDescription = ja.getString("eventBeschreibung");
                   int maxParticipants = ja.getInt("eventMaxTeilnehmer");

                   //need to get updated in the database
                   String startTime = "20:00";
                   String endTime ="21:00";
                   String eventCreator = "12345";



                   String[] dates = eventDate.split("-");


                   EventTiming eventTiming = new EventTiming(dates[0],dates[1],dates[2]);

                double distance =   distance(lat,Double.valueOf(eventLatitude),lon,Double.valueOf(eventLongitude),0.0,0.0);
                   System.out.println("Distance"+distance);
                   if(distance<50000000){



//                       Event event = new Event(eventID,eventName,eventCategory,eventTiming,startTime,endTime,
  //                             eventCreator,maxParticipants,eventLocation,eventLatitude,eventLongitude,eventDescription);

    //                mf.events.add(event);

                   }






               }




         // mf.run();





               bufferedReader.close();
               inputStream.close();
               httpURLConnection.disconnect();
               return result;
           } catch (MalformedURLException e) {
               e.printStackTrace();
           } catch (IOException e) {
               e.printStackTrace();
           } catch (JSONException e) {
               e.printStackTrace();
           }

       }
        if(type.equals("joinEvent")){
            try {
                ident = 2;
                request = "joinEvent";

                //Kategorie,
                String key = args[1];
                String eventID = args[2];



                URL url = new URL(eventAddURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                String post_data = URLEncoder.encode("event_db_request_type","UTF-8")+"="+URLEncoder.encode("joinEvent","UTF-8")+"&"
                        +URLEncoder.encode("user_key","UTF-8")+"="+URLEncoder.encode("12345","UTF-8")+"&"
                        +URLEncoder.encode("event_id","UTF-8")+"="+URLEncoder.encode(eventID,"UTF-8");




                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
                String result="";
                String line="";
                while((line = bufferedReader.readLine())!= null) {
                    result += line;

                }
                bufferedReader.close();
                inputStream.close();
                httpURLConnection.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



         else if (type.equals("loadMyEvents")) {


             ident = 1;



             try {

                 URL url = new URL(eventAddURL);
                 HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                 httpURLConnection.setRequestMethod("POST");
                 httpURLConnection.setDoOutput(true);
                 httpURLConnection.setDoInput(true);
                 OutputStream outputStream = httpURLConnection.getOutputStream();
                 BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                 String post_data = URLEncoder.encode("event_db_request_type", "UTF-8") + "=" + URLEncoder.encode("loadMyEvents", "UTF-8") + "&"
                         + URLEncoder.encode("user_key", "UTF-8") + "=" + URLEncoder.encode("12345", "UTF-8");


                 bufferedWriter.write(post_data);
                 bufferedWriter.flush();
                 bufferedWriter.close();
                 outputStream.close();

                 InputStream inputStream = httpURLConnection.getInputStream();
                 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
                 String result = "";
                 String line = "";
                 while ((line = bufferedReader.readLine()) != null) {
                     result += line;


                 }

                 bufferedReader.close();
                 inputStream.close();
                 httpURLConnection.disconnect();


                 JSONObject mainObject = new JSONObject(result);
                 JSONArray js = mainObject.getJSONArray("posts");

                 MyEventsFragment myEventsFragment = myEventsFragmentWeakReference.get();
                 for (int i = 0; i < js.length(); i++) {

                     JSONObject ja = js.getJSONObject(i);
                     JSONObject jo = ja.getJSONObject("post");
                     String eventName = jo.getString("eventName");
                     String eventLocation = jo.getString("eventLocation");
                     double eventLatitude = jo.getDouble("latitude");
                     double eventLongitude = jo.getDouble("longitude");
                     String eventDate = jo.getString("eventDate");
                     String eventID = jo.getString("eventID");
                     String eventCategory = jo.getString("eventKategorie");
                     String eventDescription = jo.getString("eventBeschreibung");
                     int maxParticipants = jo.getInt("eventMaxTeilnehmer");

                     //need to get updated in the database
                     String startTime = "20:00";
                     String endTime ="21:00";
                     String eventCreator = "12345";
                     String[] dates = eventDate.split("-");

                     EventTiming eventTiming = new EventTiming(dates[0],dates[1],dates[2]);


                  //   Event event = new Event(eventID,eventName,eventCategory,eventTiming,startTime,endTime,
                    //         eventCreator,maxParticipants,eventLocation,eventLatitude,eventLongitude,eventDescription);





                //        myEventsFragment.events.add(event);




                 }

                 myEventsFragment.run();

             } catch (ProtocolException e) {
                 e.printStackTrace();
             } catch (IOException e) {
                 e.printStackTrace();
             } catch (JSONException e) {
                 e.printStackTrace();
             }


         }
     return null;
    }

    /**
     *
     * When task is executed
     * @param aVoid
     */

    @Override
    protected void onPostExecute(String aVoid) {

        if(mainFragmentWeakReference.get()!= null){
            if(mainFragmentWeakReference.get().swipeRefreshLayout != null){
                mainFragmentWeakReference.get().swipeRefreshLayout.setRefreshing(false);
            }
        }


            if(myEventsFragmentWeakReference.get()!= null){
                if(myEventsFragmentWeakReference.get().swipeRefreshLayout != null){
                    myEventsFragmentWeakReference.get().swipeRefreshLayout.setRefreshing(false);

            }

        }



    }



    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
    }


    @Override
    public void run() {


    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     *
     */
    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371;

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance/10000000);
    }
}
