package taxiapp.com.sportmeet;

import android.os.Build;


public interface UserRegistrationHelper {



    String bytesToHex(byte[] bytes);


    String sha1Hash(String toHash);

    String getNumber();
    String getNewUserKey();

    String checkUser();

}
