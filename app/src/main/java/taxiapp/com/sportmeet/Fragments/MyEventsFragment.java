package taxiapp.com.sportmeet.Fragments;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import taxiapp.com.sportmeet.Event;
import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import taxiapp.com.sportmeet.CustomViewAdapterMainList;
import taxiapp.com.sportmeet.EventTiming;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.Server.RetrofitI;

@SuppressLint("ValidFragment")
public class
MyEventsFragment extends Fragment implements  Runnable, SwipeRefreshLayout.OnRefreshListener {
    public ArrayList<Event> events = new ArrayList<>();
    private EventFragment.JoinEventListener fragmentInterfaceListener;

    public CustomViewAdapterMainList mAdapter;
    private ListView listView;
    public SwipeRefreshLayout swipeRefreshLayout;
    private static final String DESCRIBABLE_KEY = "userID";
    private String userID ="";

    public static MainFragment newInstance(MainActivity mainActivity) {
        MainFragment fragment = new MainFragment();
        Bundle bundle = new Bundle();
        bundle.putString(DESCRIBABLE_KEY, mainActivity.user.getId());
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.myevent_list_fragment, container, false);

        userID = getArguments().getString("userID");
        swipeRefreshLayout = v.findViewById(R.id.swiperefresh2);

        swipeRefreshLayout.setOnRefreshListener(this);

        mAdapter = new CustomViewAdapterMainList(getContext(),events, userID);
        listView  = v.findViewById(R.id.listview);
        listView.setAdapter(mAdapter);
        listView.setEmptyView(v.findViewById(android.R.id.empty));


        EventBus eventBus = EventBus.getDefault();

        eventBus.register(this);




        onRefresh();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //preventing double click
                Fragment test =  getFragmentManager().findFragmentByTag("event");


                if (test != null && test.isVisible()) {
                    return;
                }


                Event currentEvent  = events.get(position);


                Fragment jointFragment = new JoinedEventCover();
                Bundle bundle = new Bundle();
                bundle.putString(DESCRIBABLE_KEY, userID);
                bundle.putSerializable("event", currentEvent);
                jointFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction =getFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.frameLayoutAdd,jointFragment,"event").addToBackStack("1");
                fragmentTransaction.commit();
            }
        });


        return v;
    }

    @Override
    public void run() {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRefresh() {

        loadData();

    }
    public void loadData() {
       Retrofit retrofit = new Retrofit.Builder().baseUrl(RetrofitI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitI retrofitI = retrofit.create(RetrofitI.class);
        Call<ArrayList<Event>> call = retrofitI.loadMyEvents("loadMyEvents",userID);
        call.enqueue(new Callback<ArrayList<Event>>() {
            @Override
            public void onResponse(Call<ArrayList<Event>> call, Response<ArrayList<Event>> response) {
                events = response.body();
                if (events.size() > 0) {
                    if (response.body() != null) {

                        for (Event x : events) {

                            String[] dates = x.eventDate.split("-");
                            EventTiming date = new EventTiming(dates[0], dates[1], dates[2]);
                            x.setDate(date);

                            mAdapter = new CustomViewAdapterMainList(getContext(), events, userID);
                            listView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);

                        }
                        return;
                    }
                }
                mAdapter = new CustomViewAdapterMainList(getContext(), events, userID);
                listView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<ArrayList<Event>> call, Throwable t) {
                System.out.println("FAIL");

            }
        });


    }

    @Subscribe
    public void onEvent(JoinEventMessages joinEventMessages){
     onRefresh();
    }



}
