package taxiapp.com.sportmeet.Chat;
import java.io.Serializable;

/**
 * Created by Belal on 5/29/2016.
 */
public class Message  {
    private String usersId;
    private String message;
    private String sentAt;
    private String userName;


    public Message(String usersId, String message, String sentAt, String userName) {
        this.usersId = usersId;
        this.message = message;
        this.sentAt = sentAt;
        this.userName = userName;

    }

    public String getUserName() {
        return userName;
    }

    public String getUsersId() {
        return usersId;
    }

    public String getMessage() {
        return message;
    }

    public String getSentAt() {
        return sentAt;
    }


}