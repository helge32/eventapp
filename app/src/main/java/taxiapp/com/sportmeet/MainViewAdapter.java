package taxiapp.com.sportmeet;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import taxiapp.com.sportmeet.Fragments.MainFragment;
import taxiapp.com.sportmeet.Fragments.MyEventsFragment;

public class MainViewAdapter extends FragmentPagerAdapter {
    Fragment mainFragment;
    Fragment myEventsFragment;


    public MainViewAdapter(FragmentManager fm, Fragment mf, Fragment mef) {
        super(fm);
        this.mainFragment = mf;
        this.myEventsFragment = mef;

    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:


                return mainFragment;

                case 1:


                return myEventsFragment;
        }

        return null;
    }


    @Override
    public int getCount() {
        return 2;
    }
}
