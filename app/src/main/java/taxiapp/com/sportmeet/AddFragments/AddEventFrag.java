package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.auth.FirebaseAuth;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import taxiapp.com.sportmeet.Fragments.MainFragment;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.Server.RetrofitI;
import taxiapp.com.sportmeet.Server.ServerKlasse;
import taxiapp.com.sportmeet.User;

@SuppressLint("ValidFragment")
public class AddEventFrag extends Fragment  {
private   Fragment fragment;
public HashMap<String,String> eventDetails = new HashMap<>();
public Button weiter;
private MainActivity mainActivity;
public int flag;


public AddEventFrag(MainActivity mainActivity){


    this.mainActivity = mainActivity;
}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_event_frag_background, container, false);
        weiter = v.findViewById(R.id.addBtn);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        fragment  = new AddEventFragment1(this);
        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
        fragmentTransaction.replace(R.id.slider,fragment).commit();





        FragmentTransaction fragmentTransaction1 = getChildFragmentManager().beginTransaction();
      //




        return  v;

    }

    public  void switchLayout(int i){

        switch (i){
            case 1:
                fragment  = new AddEventFragment1(this);
                break;
            case 2:
                fragment = new AddEventFragment2(this);
                break;
            case 3:
                fragment = new AddEventFragment3(this);
                break;
            case 4:
                fragment = new AddEventFragment4(this);
                break;
            case 5:
                fragment = new AddEventFragment5(this);
                break;
            case 6:
                fragment = new AddEventFragment6(this);
                break;
            case 7:
                fragment = new AddEventFragment7(this);
                break;
            case 8:
                fragment = new AddEventFragment8(this);
                break;






        }

        FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right,R.anim.exit_to_left);
        fragmentTransaction.replace(R.id.slider,fragment).addToBackStack(null).commit();
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    public void addEventFinished(){

        FragmentManager fragmentManagerchild = getChildFragmentManager();
        fragmentManagerchild.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);



    }

public void addEvent(){


    String name = eventDetails.get("name");
    String location = eventDetails.get("location");
    String category = eventDetails.get("category");
    String date = eventDetails.get("date");
    String description = eventDetails.get("description");
    String latitude = eventDetails.get("latitude");
    String longitude = eventDetails.get("longitude");
    String maxParticipants = eventDetails.get("numberParticipants");
    String city = eventDetails.get("city");
   Retrofit retrofit = new Retrofit.Builder().baseUrl(RetrofitI.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .build();

    FirebaseAuth mAuth = FirebaseAuth.getInstance();


    RetrofitI retrofitI = retrofit.create(RetrofitI.class);

    Call<Void> call = retrofitI.addEvent(
            "addEvent",date,maxParticipants,description,category,name,location, latitude, longitude, "18:00",
            "20:00", mAuth.getUid(), city);

    call.enqueue(new Callback<Void>() {
        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {
           // mainFragment.setFeedback("You joined the event");
        }

        @Override
        public void onFailure(Call<Void> call, Throwable t) {

        }
    });

}

}
