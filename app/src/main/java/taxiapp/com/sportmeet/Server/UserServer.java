package taxiapp.com.sportmeet.Server;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.Welcome.WelcomeActivity;

public class UserServer extends AsyncTask<String, String, String> {

    private MainActivity mainActivity;
    private WelcomeActivity welcomeActivity;
    String userStatus;

    public UserServer(MainActivity mainActivity, WelcomeActivity welcomeActivity) {
        this.welcomeActivity = welcomeActivity;
        this.mainActivity = mainActivity;
    }

    @Override
    protected String doInBackground(String... strings) {

        String type = strings[0];



        if (type.equals("checkUser")) {

            String userSHA = strings[1];


            try {
                URL url = new URL("https://eventmeetapp.com/user.php");

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                String post_data = URLEncoder.encode("event_db_request_type", "UTF-8") + "=" + URLEncoder.encode("checkUser", "UTF-8") + "&"
                        + URLEncoder.encode("user_key", "UTF-8") + "=" + URLEncoder.encode(userSHA, "UTF-8");


                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
                String result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;

                }
                System.out.println(result);
                JSONObject mainObject = new JSONObject(result);

                userStatus = mainObject.getString("bool");



                if(userStatus.equals("1")){

                    if(welcomeActivity != null){
                        welcomeActivity.enterApp();
                    }


                }
                else{


                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    return  userStatus;
    }

    @Override
    protected void onPostExecute(String s) {
    }
}