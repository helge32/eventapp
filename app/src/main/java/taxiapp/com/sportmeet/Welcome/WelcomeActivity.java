package taxiapp.com.sportmeet.Welcome;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;


import androidx.annotation.NonNull;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import taxiapp.com.sportmeet.Fragments.MainFragment;
import taxiapp.com.sportmeet.Fragments.MyEventsFragment;
import taxiapp.com.sportmeet.MainViewAdapter;
import taxiapp.com.sportmeet.RegisterTools;

import androidx.core.app.ActivityCompat;

import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.concurrent.TimeUnit;

import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.RegisterTools;
import taxiapp.com.sportmeet.Server.RetrofitI;
import taxiapp.com.sportmeet.Server.UserServer;

public class WelcomeActivity extends RegisterTools {
    private static final int REQUEST_READ_PHONE_STATE = 1;
    private static final int REQUEST_FINE_LOCATION = 2;
    private Retrofit retrofit;
    private RetrofitI retrofitI;
    private Button registerBtn;
    private FrameLayout frameLayout;
    private FusedLocationProviderClient mFusedLocationClient;
    private   Fragment locationFragment2;


    private  String key = getNewUserKey();
    private String number;


    private WelcomeFragment1 locationFragment;

    private String currentUsername = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_layout);


        frameLayout = findViewById(R.id.frameWelcome);

        retrofit = new Retrofit.Builder().baseUrl(RetrofitI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        retrofitI = retrofit.create(RetrofitI.class);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        locationFragment = new WelcomeFragment1();

        transaction.add(R.id.frameWelcome, locationFragment).commit();

        registerBtn = findViewById(R.id.welcomeButton);
        registerBtn.setText("Create Account");

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (registerBtn.getText().toString().equals("Create Account")){

                    currentUsername = locationFragment.getFromUser();


                    if(!currentUsername.equals("") ){

                        login();
                    }


                }
                if (registerBtn.getText().toString().equals("Allow Location")) {
                    ActivityCompat.requestPermissions(WelcomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 2);

                }

                if(currentUsername.equals("")){
                    locationFragment.setError();
                }
            }

        });


    }

    public void enterApp() {

        Intent enter = new Intent(this, MainActivity.class);
        startActivity(enter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 2) {


            if (ActivityCompat.checkSelfPermission(WelcomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            }
            if (ActivityCompat.checkSelfPermission(WelcomeActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(WelcomeActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                            PackageManager.PERMISSION_GRANTED) {

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(WelcomeActivity.this);

                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(WelcomeActivity.this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    Intent main = new Intent(WelcomeActivity.this, MainActivity.class);
                                    startActivity(main);
                                    finish();
                                }
                            }

                        });
            }

        }

    }

    @Override
    public void onBackPressed() {
        return;
    }

    public void login() {

        Fragment test = getSupportFragmentManager().findFragmentByTag("locationFrag");
        final String currentKey = checkUser();
        if (test != null && test.isVisible()) {
            return;
        } else {


            Call<JsonElement> call = retrofitI.checkUser("checkUser", currentKey);
            call.enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                    assert response.body() != null;
                    JsonObject userStatus = response.body().getAsJsonObject();
                    String bool = userStatus.get("bool").toString().replace("\"", "");


                    if (bool.equals("0")) {
                        locationFragment.progressBar.setVisibility(View.VISIBLE);
                        createUser(currentUsername);



                    } else {
                        key = currentKey;
                        createUser(currentUsername);
                        locationFragment.progressBar.setVisibility(View.VISIBLE);
                        SharedPreferences prefs = getSharedPreferences("user", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("key", key);
                        editor.putString("username", currentUsername);
                        editor.commit();
                        locationFragment.progressBar.setVisibility(View.GONE);
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        locationFragment2 = new WelcomeFragment2();
                        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                        transaction.replace(R.id.frameWelcome, locationFragment2, "locationFrag").commit();
                        registerBtn.setText("Allow Location");
                    }
                }


                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {

                }
            });
        }
    }

    private void createUser(final String username) {


        Call<Void> call = retrofitI.createUser("registerUser", key, username);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {


                SharedPreferences prefs = getSharedPreferences("user", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("key", key);
                editor.putString("username", currentUsername);
                editor.commit();
                locationFragment.progressBar.setVisibility(View.GONE);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                locationFragment2 = new WelcomeFragment2();
                transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                transaction.replace(R.id.frameWelcome, locationFragment2, "locationFrag").commit();
                registerBtn.setText("Allow Location");


            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

}