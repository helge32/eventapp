package taxiapp.com.sportmeet;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import taxiapp.com.sportmeet.EventTiming;

public class Event implements Serializable {

    @SerializedName("eventID")
    public String eventID;
    @SerializedName("eventCreator")
    public String eventCreator;
    @SerializedName("eventName")
    public String eventName;
    @SerializedName("eventKategorie")
    public String eventKategorie;
    @SerializedName("eventDate")
    public String eventDate;
    @SerializedName("eventMaxTeilnehmer")
    public int eventMaxTeilnehmer;
    @SerializedName("eventLocation")
    public String eventLocation;
    @SerializedName("latitude")
    public double latitude;
    @SerializedName("longitude")
    public double longitude;
    @SerializedName("eventBeschreibung")
    public String eventBeschreibung;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;
    public EventTiming date;
    @SerializedName("currentJoin")
    private Integer currentJoin;
    @SerializedName("eventCity")
    private String city;


    public Event(String eventID, String eventCreator, String eventName, String eventKategorie, String eventDate, int eventMaxTeilnehmer, String eventLocation, double latitude, double longitude, String eventBeschreibung, String startTime, String endTime, Integer currentJoin) {
        this.eventID = eventID;
        this.eventCreator = eventCreator;
        this.eventName = eventName;
        this.eventKategorie = eventKategorie;
        this.eventDate = eventDate;
        this.eventMaxTeilnehmer = eventMaxTeilnehmer;
        this.eventLocation = eventLocation;
        this.latitude = latitude;
        this.longitude = longitude;
        this.eventBeschreibung = eventBeschreibung;
        this.startTime = startTime;
        this.endTime = endTime;
        this.currentJoin = currentJoin;
        this.city = city;


    }

    public String getEventID() {
        return eventID;
    }

    public String getEventCreator() {
        return eventCreator;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventKategorie() {
        return eventKategorie;
    }

    public Integer getCurrentJoin() {
        return currentJoin;
    }

    public void setCurrentJoin(Integer currentJoin) {
        this.currentJoin = currentJoin;
    }

    public String getEventDate() {
        return eventDate;
    }

    public int getEventMaxTeilnehmer() {
        return eventMaxTeilnehmer;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getEventBeschreibung() {
        return eventBeschreibung;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public EventTiming getDate() {
        return date;
    }

    public void setDate(EventTiming date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
