package taxiapp.com.sportmeet.Fragments;

import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import taxiapp.com.sportmeet.CustomViewAdapterMainList;
import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.EventTiming;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.Server.RetrofitI;


@SuppressLint("ValidFragment")
public class MainList extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    public CustomViewAdapterMainList mAdapter;


    private String userID;
    public Fragment eventFragment;
    private Fragment feedback = new FeedbackJoined();
    public Fragment addEventFragment;
    private final String LOG_TAG = "tag";
    private static final String DESCRIBABLE_KEY = "userID";
    private boolean isClicked;
    private Fragment myEventsFragment = new MyEventsFragment();
    private FirebaseAuth firebaseAuth;

    private Retrofit retrofit;
    private RetrofitI retrofitI;

    private TextView empty;

    ListView listView;
    public View view;
    public ArrayList<Event> events = new ArrayList<>();

    public SwipeRefreshLayout swipeRefreshLayout;
    double latitude =0.0;
    double longitude = 0.0;

    public TextView myEventsTxt;
    public TextView eventsTxt;
    public ViewPager viewPager;


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_list, container, false);
        userID = getArguments().getString("userID");
        getLocation();
        retrofit = new Retrofit.Builder().baseUrl(RetrofitI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        retrofitI = retrofit.create(RetrofitI.class);


        swipeRefreshLayout = v.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(this);


        empty = v.findViewById(R.id.emptyTxt);


        listView = v.findViewById(R.id.listView);

        EventBus eventBus = EventBus.getDefault();
        eventBus.register(this);

        onRefresh();

        v.setOnTouchListener(new OnSwipeTouchListener(getContext()){

            public void onSwipeTop() {

            }
            public void onSwipeRight() {

            }
            public void onSwipeLeft() {

                //  m.onClick(m.myEventsTxt);

            }
            public void onSwipeBottom() {

            }


        });



        /*
            Event kann geaddet werden
            Fragment öffent sich

           */

        listView = v.findViewById(R.id.listView);

        System.out.println("eventsList" + events);


        mAdapter = new CustomViewAdapterMainList(getContext(), events, userID);
        listView.setAdapter(mAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {



                eventFragment = new EventFragment();
                Bundle b = new Bundle();
                b.putString("userID", userID);
                b.putSerializable("event", events.get(position));
                eventFragment.setArguments(b);

                //preventing doubleTab
                Fragment test = getFragmentManager().findFragmentByTag("event");


                if (test != null && test.isVisible()) {
                    return;
                } else {
                    FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();

                    fragmentTransaction.add(R.id.eventFrame, eventFragment, "event").addToBackStack("1");

                    fragmentTransaction.commit();
                }


            }




        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {



                return false;
            }
        });




        return v;
    }



    public void loadData() {


        Call<ArrayList<Event>> call = retrofitI.getEvents("loadEvents", userID);
        call.enqueue(new Callback<ArrayList<Event>>() {
            @Override
            public void onResponse(Call<ArrayList<Event>> call, Response<ArrayList<Event>> response) {
                events = response.body();

                if (events.size() > 0) {
                    for (Event x : events) {
                        double distance = distance(latitude, Double.valueOf(x.latitude), longitude, Double.valueOf(x.longitude), 0.0, 0.0);
                        System.out.println("Distance" + distance);
                        if (distance < 5000) {
                            String[] dates = x.eventDate.split("-");

                            EventTiming date = new EventTiming(dates[0], dates[1], dates[2]);
                            x.setDate(date);


                            mAdapter = new CustomViewAdapterMainList(getContext(), events, userID);
                            listView.setAdapter(mAdapter);
                            listView.setEmptyView(empty);
                            mAdapter.notifyDataSetChanged();
                            swipeRefreshLayout.setRefreshing(false);

                        }
                    }
                    return;
                }
                mAdapter = new CustomViewAdapterMainList(getContext(), events, userID);
                listView.setAdapter(mAdapter);
                listView.setEmptyView(empty);
                mAdapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<ArrayList<Event>> call, Throwable t) {
                System.out.println("FAIL");
                swipeRefreshLayout.setRefreshing(false);

            }
        });


    }


    @Override
    public void onRefresh() {
        Log.i(LOG_TAG, "onRefresh called from SwipeRefreshLayout");

        loadData();

    }


    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {

        final int R = 6371;

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters

        double height = el1 - el2;

        distance = Math.pow(distance, 2) + Math.pow(height, 2);

        return Math.sqrt(distance / 10000000);
    }

    private void getLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        if(ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {

            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                });

    }

    @Subscribe
    public void onEvent(JoinEventMessages joinEventMessages){
            onRefresh();
            setFeedback();
    }



    public void setFeedback() {

        final FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.exit, R.anim.exit_to_left);

        fragmentTransaction.replace(R.id.feedbackFrame, feedback, "feedback").commit();


        FeedbackJoined fragment = (FeedbackJoined) getChildFragmentManager().findFragmentByTag("feedback");
        Handler mHandler = new Handler();
        Runnable mRunnable = new Runnable() {


            @Override
            public void run() {


                // TODO Auto-generated method stub
                Fragment fragment = getChildFragmentManager().findFragmentByTag("feedback");
                FragmentTransaction fragmentTransaction1 = getChildFragmentManager().beginTransaction();
                fragmentTransaction1.setCustomAnimations(R.anim.exit, R.anim.pop);
                fragmentTransaction1.replace(R.id.feedbackFrame, new Fragment()).commit();
            }

        };
        mHandler.postDelayed(mRunnable, 10 * 300);
    }




}
