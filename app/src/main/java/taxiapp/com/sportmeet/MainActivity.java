package taxiapp.com.sportmeet;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.os.PersistableBundle;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.internal.NavigationMenuItemView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import java.io.IOException;
import java.io.Serializable;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.zip.Inflater;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import taxiapp.com.sportmeet.AddFragments.AddEventFrag;
import taxiapp.com.sportmeet.Fragments.MainFragment;
import taxiapp.com.sportmeet.Fragments.MyEventsFragment;
import taxiapp.com.sportmeet.Fragments.PhoneVerification;
import taxiapp.com.sportmeet.Fragments.SettingFragment;
import taxiapp.com.sportmeet.Server.RetrofitI;
import taxiapp.com.sportmeet.Server.UserServer;
import taxiapp.com.sportmeet.Welcome.WelcomeActivity;
import taxiapp.com.sportmeet.Welcome.WelcomeFragment2;
import taxiapp.com.sportmeet.Welcome.WelcomePageViewAdapter;

public class MainActivity extends RegisterTools {
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;


    private BottomNavigationView bottomNavigationView;
    private final String TAG = "1";

    private FusedLocationProviderClient mFusedLocationClient;
    private Retrofit retrofit;
    private RetrofitI retrofitI;

    private ImageButton settingsBtn;
    private ImageButton eventBtn;

    private Fragment mainFragment = new MainFragment();
    private Fragment fragment3 = new SettingFragment();



    private Fragment active;

    public double lat;
    public double lon;



    public User user;

    private String key;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);




        setContentView(R.layout.activity_main);
        bottomNavigationView = findViewById(R.id.navigation);


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        retrofit = new Retrofit.Builder().baseUrl(RetrofitI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        retrofitI = retrofit.create(RetrofitI.class);






        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.profile:
                        getSupportFragmentManager().beginTransaction().hide(active).show(fragment3).commit();
                        active = fragment3;
                        return true;


                    case R.id.events:
                        getSupportFragmentManager().beginTransaction().hide(active).show(mainFragment).commit();
                        active = mainFragment;
                        return true;



                    case R.id.eventAddFrag:
                        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

                            Fragment fragment = new AddEventFrag(MainActivity.this);
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.add(R.id.frameLayoutAdd, fragment).addToBackStack("1").commit();


                        } else {

                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                            PhoneVerification phoneVerification = new PhoneVerification();
                            transaction.add(R.id.frameLayoutAdd, phoneVerification).addToBackStack("1").commit();


                        }


                        return true;

                }

                return false;
            }
        });

        createUI();
    }




    private void updateProfile(String key) {
        Call<ArrayList<User>> call = retrofitI.updateProfile("updateUser", key);
        call.enqueue(new Callback<ArrayList<User>>() {
            @Override
            public void onResponse(Call<ArrayList<User>> call, Response<ArrayList<User>> response) {
                ArrayList<User> userList = response.body();
                assert userList != null;
                user = userList.get(0);
                if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                                PackageManager.PERMISSION_GRANTED) {

                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(MainActivity.this);

                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {

                                        firebaseAuth = FirebaseAuth.getInstance();
                                        Bundle b = new Bundle();
                                        b.putString("userID", firebaseAuth.getUid());
                                        mainFragment.setArguments(b);



                                        lat = location.getLatitude();
                                        lon = location.getLongitude();

                                        active=mainFragment;
                                        getSupportFragmentManager().beginTransaction().add(R.id.frameLayoutSettings, mainFragment).commitAllowingStateLoss();
                                        getSupportFragmentManager().beginTransaction().add(R.id.frameLayoutSettings, fragment3, "3").hide(fragment3).commitAllowingStateLoss();

                                    }
                                }
                            });

                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 2);


                }
            }

            @Override
            public void onFailure(Call<ArrayList<User>> call, Throwable t) {

            }
        });

    }

    public void startWelcome() {


        Intent welcome = new Intent(this, WelcomeActivity.class);
        startActivity(welcome);
        finish();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 2) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    lat = location.getLatitude();
                                    lon = location.getLongitude();
                                }
                            }
                        });
                getSupportFragmentManager().beginTransaction().add(R.id.frameLayoutSettings, mainFragment).commit();
                getSupportFragmentManager().beginTransaction().add(R.id.frameLayoutSettings, fragment3, "3").hide(fragment3).commit();

            }
        }
    }

    @Override
    public void onBackPressed() {
        // if there is a fragment and the back stack of this fragment is not empty,
        // then emulate 'onBackPressed' behaviour, because in default, it is not working
        FragmentManager fm = getSupportFragmentManager();
        for (Fragment frag : fm.getFragments()) {
            if (frag.isVisible()) {
                FragmentManager childFm = frag.getChildFragmentManager();
                if (childFm.getBackStackEntryCount() > 0) {
                    childFm.popBackStack();
                    return;
                }
            }
        }
        super.onBackPressed();
    }

    @SuppressLint("ResourceAsColor")



    private void createUI(){
        final String key = checkUser();


        Call<JsonElement> call = retrofitI.checkUser("checkUser", key);
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                assert response.body() != null;
                JsonObject userStatus = response.body().getAsJsonObject();
                String bool = userStatus.get("bool").toString().replace("\"", "");



                if (bool.equals("0")) {

                    Intent welcome = new Intent(MainActivity.this, WelcomeActivity.class);
                    startActivity(welcome);
                    finish();
                }else{
                    updateProfile(checkUser());


                }


            }



            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

                System.out.println("");
            }
        });

    }


}


