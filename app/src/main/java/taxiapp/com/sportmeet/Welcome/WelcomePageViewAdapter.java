package taxiapp.com.sportmeet.Welcome;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class WelcomePageViewAdapter extends FragmentPagerAdapter {
    public WelcomePageViewAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

      switch (position){
          case 0:
            return new WelcomeFragment1();
          case 1:
              return new WelcomeFragment2();
      }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
