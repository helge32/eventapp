package taxiapp.com.sportmeet;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;


import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import java.sql.Date;
import java.util.ArrayList;

public class CustomViewAdapterMainList extends BaseAdapter {


    private Context mContext;
    private ArrayList<Event> events;
    private String userID;



    public CustomViewAdapterMainList(Context mContext, ArrayList<Event> events, String user)
    {
        this.userID = user;
        this.events = events;
        this.mContext = mContext;

    }

    @Override
    public int getCount() {
        if(events != null)
            return events.size();
        return 0;
    }

    @Override
    public Object getItem(int i) {
        return events.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.O)

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext,R.layout.eventlist_item_list, null);
        RelativeLayout relativeLayout = v.findViewById(R.id.itemBack);

        TextView nameTextView = v.findViewById(R.id.eventName);
        TextView categoryTextView = v.findViewById(R.id.category);
        TextView locationTextView = v.findViewById(R.id.location);
        TextView startEndTimeTextView = v.findViewById(R.id.time);
        TextView maxParticipantsTextView = v.findViewById(R.id.participants);
        //Dates
        TextView dayWrittenTextView = v.findViewById(R.id.dayWritten);
        TextView dayNumberTextView  = v.findViewById(R.id.dayNumber);
        TextView monthWrittenTextView = v.findViewById(R.id.monthWrtitten);

        Date date = new Date(Integer.valueOf(events.get(i).date.year),Integer.valueOf(events.get(i).getDate().month)-1,Integer.valueOf(events.get(i).getDate().day)-1);

        String dayWritten = (String) android.text.format.DateFormat.format("EEEE", date);

        String monthWritten = (String) android.text.format.DateFormat.format("MMM", date);

        String[] dates = events.get(i).date.toString().split("-");



        nameTextView.setText(events.get(i).getEventName());
        categoryTextView.setText(events.get(i).getEventKategorie());
        locationTextView.setText(events.get(i).getCity());
        startEndTimeTextView.setText(events.get(i).getStartTime()+" - "+events.get(i).getEndTime());
        maxParticipantsTextView.setText(String.valueOf(events.get(i).getCurrentJoin()  +" / "+ events.get(i).getEventMaxTeilnehmer()+ " participants"));

        dayWrittenTextView.setText(dayWritten.substring(0,3));
        dayNumberTextView.setText(String.valueOf(events.get(i).date.day));
        monthWrittenTextView.setText(monthWritten);

        if(events.get(i).getEventCreator().equals(userID)){
            relativeLayout.setBackgroundResource(R.drawable.ownshadow);

        }




        if(events.get(i).getEventKategorie().equals("Sport")){
            categoryTextView.setTextColor(ContextCompat.getColor(mContext, R.color.sportcolor));
            }
        else if(events.get(i).getEventKategorie().equals("Party")){
            categoryTextView.setTextColor(ContextCompat.getColor(mContext, R.color.partycolor));
        }
        else if(events.get(i).getEventKategorie().equals("Other")){
            categoryTextView.setTextColor(ContextCompat.getColor(mContext, R.color.anderescolor));
        }
        else if(events.get(i).getEventKategorie().equals("Culture")){
            categoryTextView.setTextColor(ContextCompat.getColor(mContext, R.color.culturecolor));
        }




        return v;
    }


}