package taxiapp.com.sportmeet.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import taxiapp.com.sportmeet.AddFragments.AddEventFrag;
import taxiapp.com.sportmeet.CustomViewAdapterMainList;
import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.EventTiming;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.MainViewAdapter;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.Server.RetrofitI;
import taxiapp.com.sportmeet.Server.ServerKlasse;
import taxiapp.com.sportmeet.TestEvent;



@SuppressLint("ValidFragment")
public class MainFragment extends Fragment implements View.OnClickListener {

    public CustomViewAdapterMainList mAdapter;

    private String userID;
    public Fragment eventFragment;
    public Fragment addEventFragment;
    private final String LOG_TAG = "tag";
    private static final String DESCRIBABLE_KEY = "userID";
    private boolean isClicked;
    private Fragment myEventsFragment = new MyEventsFragment();
    private Fragment mainList = new MainList();
    private FirebaseAuth firebaseAuth;

    private Retrofit retrofit;
    private RetrofitI retrofitI;

    private TextView empty;

    ListView listView;
    public View view;
    public ArrayList<Event> events = new ArrayList<>();

    public SwipeRefreshLayout swipeRefreshLayout;
    double latitude = 0.0;
    double longitude = 0.0;

    public TextView myEventsTxt;
    public TextView eventsTxt;
    public ViewPager viewPager;


    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_event_list_fragment, container, false);
        userID = getArguments().getString("userID");

        eventsTxt = v.findViewById(R.id.eventsTxt);
        myEventsTxt = v.findViewById(R.id.myEventsTxt);
        myEventsTxt.setOnClickListener(this);
        eventsTxt.setOnClickListener(this);

        firebaseAuth = FirebaseAuth.getInstance();
        Bundle b = new Bundle();
        b.putString("userID", firebaseAuth.getUid());
        myEventsFragment.setArguments(b);
        mainList.setArguments(b);

        viewPager = (ViewPager) v.findViewById(R.id.mainViewPager);
        MainViewAdapter mainViewAdapter = new MainViewAdapter(getFragmentManager(), mainList, myEventsFragment);
        viewPager.setAdapter(mainViewAdapter);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 0:
                        eventsTxt.setTextSize(30);
                        eventsTxt.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                        myEventsTxt.setTextSize(20);
                        myEventsTxt.setTextColor(0xFF000000);
                        break;


                    case 1:
                        myEventsTxt.setTextSize(30);
                        myEventsTxt.setTextColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                        eventsTxt.setTextSize(20);
                        eventsTxt.setTextColor(0xFF000000);
                        break;
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout tabLayout = (TabLayout) v.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager, true);


        return v;
    }

    @Override
    public void onClick(View v) {

    }



}
