package taxiapp.com.sportmeet;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomViewAdapterCategory extends BaseAdapter {



    ArrayList<String> categories;
    Context mContext;





    public CustomViewAdapterCategory(Context mContext,ArrayList<String> categories)
    {
        this.categories = categories;
        this.mContext = mContext;

    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.O)

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(mContext,R.layout.category_listview_item, null);


        TextView categoryTextView = v.findViewById(R.id.categoryTextVIewItem);


        categoryTextView.setText(categories.get(i));


        return v;
    }


}

