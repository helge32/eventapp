package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import android.widget.DatePicker;


import java.lang.ref.WeakReference;
import java.util.Calendar;

import androidx.fragment.app.DialogFragment;

@SuppressLint("ValidFragment")
public  class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    private WeakReference<AddEventFragment4> weakReference;

    public DatePickerFragment(AddEventFragment4 addEventFragment4){
        weakReference = new WeakReference<>(addEventFragment4);


    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);


        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {




    }
}