package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.textfield.TextInputEditText;
import com.jaredrummler.materialspinner.MaterialSpinner;

import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;

import static android.content.Context.INPUT_METHOD_SERVICE;

@SuppressLint("ValidFragment")
public class AddEventFragment1 extends Fragment {

    private TextInputEditText eventName;
    private TextInputEditText start;
    private TextInputEditText end;
    private TextInputEditText datePick;
    private TextInputEditText description;
    private MaterialSpinner spinner;
    private ArrayAdapter<CharSequence> spinnerAdapter;
    private ArrayList<TextInputEditText> editTexts = new ArrayList<>();


    private AddEventFrag aF;

    @SuppressLint("ValidFragment")
    public AddEventFragment1(AddEventFrag aF) {

        this.aF = aF;


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout1, container, false);
        datePick = v.findViewById(R.id.date);
        eventName = v.findViewById(R.id.name);
        start = v.findViewById(R.id.startTime);
        end = v.findViewById(R.id.endTime);
        spinner = v.findViewById(R.id.category);
        description = v.findViewById(R.id.description);

        editTexts.add(eventName);
        editTexts.add(start);
        editTexts.add(end);
        editTexts.add(datePick);
        editTexts.add(description);


        spinnerAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.categories, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
            }
        });


        final SimpleDateFormat dateFormat = new SimpleDateFormat(" dd.MM.");
        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                datePick.setText(dateFormat.format(myCalendar.getTime()));



            }

        };

        datePick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                try {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                } catch (Exception e) {
                    // TODO: handle exception
                }
                new DatePickerDialog(getContext(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (isEmpty(editTexts)) {
                    aF.eventDetails.put("name", eventName.getText().toString());
                    aF.eventDetails.put("date", new SimpleDateFormat("yyyy-MM-dd").format(myCalendar.getTime()));
                    aF.eventDetails.put("startTime", start.getText().toString());
                    aF.eventDetails.put("endTime", end.getText().toString());
                    aF.eventDetails.put("category", spinnerAdapter.getItem(spinner.getSelectedIndex()).toString());
                    aF.eventDetails.put("description", description.getText().toString());
                    aF.switchLayout(2);

                }

            }
        });


        return v;
    }

    private boolean isEmpty(ArrayList<TextInputEditText> textInputEditTexts) {
        boolean valid = true;

        for (TextInputEditText textInputEditText : textInputEditTexts) {

            if (textInputEditText.getText().toString().trim().length() == 0) {
                valid = false;
                textInputEditText.setError("Required");

            }
        }

        return valid;

    }

}
