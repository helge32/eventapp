package taxiapp.com.sportmeet.Server;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import androidx.recyclerview.widget.RecyclerView;
import taxiapp.com.sportmeet.Chat.Message;
import taxiapp.com.sportmeet.Chat.ChatEventFragment;

public class ChatServer extends AsyncTask<String, String, String> {
    private Context c;
    private String request;
    private RecyclerView.Adapter adapter;


    private ChatEventFragment chatEventFragmentWeakReference;


    public ChatServer(Context context, ChatEventFragment weakReference) {

        chatEventFragmentWeakReference = weakReference;

        c = context;


    }

    String urlString = "http://eventmeetapp.com/chat.php";

    @Override
    protected String doInBackground(String... strings) {

        request = strings[0];
        if (request.equals("getMessages")) {

            try {
                URL url = null;

                String eventId = strings[1];
                url = new URL(urlString);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                httpURLConnection.setRequestMethod("POST");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                String post_data = URLEncoder.encode("event_db_request_type", "UTF-8") + "=" + URLEncoder.encode("getMessages", "UTF-8")
                        + "&" + URLEncoder.encode("event_ID", "UTF-8") + "=" + URLEncoder.encode(eventId, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                InputStream inputStream = httpURLConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.ISO_8859_1));
                String result = "";
                String line = "";
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;

                }
                JSONObject mainObject = new JSONObject(result);
                JSONArray js = mainObject.getJSONArray("posts");


                chatEventFragmentWeakReference.messagesBuffer.clear();

                for (int i = 0; i < js.length(); i++) {

                    JSONObject ja = js.getJSONObject(i);
                    JSONObject jo = ja.getJSONObject("post");


                    Message messagObject = new Message(jo.getString("UserID"),jo.getString("Message"), jo.getString("Time"),"");
                   chatEventFragmentWeakReference.messagesBuffer.add(messagObject);

                }


                return "";


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (request.equals("sendMessage")) {

            String eventId = strings[2];
            URL url = null;
            String message = strings[1];
            String userID = strings[3];
            try {
                url = new URL(urlString);

                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

                httpURLConnection.setRequestMethod("POST");

                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                OutputStream outputStream = httpURLConnection.getOutputStream();
                BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
                String post_data = URLEncoder.encode("event_db_request_type", "UTF-8") + "=" + URLEncoder.encode("sendMessage", "UTF-8")
                        + "&" + URLEncoder.encode("user_key", "UTF-8") + "=" + URLEncoder.encode(userID, "UTF-8")
                        + "&" + URLEncoder.encode("event_ID", "UTF-8") + "=" + URLEncoder.encode(eventId, "UTF-8")
                        + "&" + URLEncoder.encode("message", "UTF-8") + "=" + URLEncoder.encode(message, "UTF-8");

                bufferedWriter.write(post_data);
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStream.close();
                httpURLConnection.getInputStream();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        return null;
    }



    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if (request.equals("getMessages")) {

        }
        if (request.equals("sendMessage")) {



        }
    }
}
