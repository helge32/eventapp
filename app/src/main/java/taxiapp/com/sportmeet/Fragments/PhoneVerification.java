package taxiapp.com.sportmeet.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import kotlin.reflect.jvm.internal.impl.descriptors.PropertyGetterDescriptor;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.Welcome.WelcomeActivity;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PhoneVerification extends Fragment {

    private TextInputEditText otp;
    private ProgressBar progressBar;
    private String number;
    private String mVerificationId;
    private Button verifiyBtn;
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.phone_verification, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        progressBar = v.findViewById(R.id.progress);
        otp = v.findViewById(R.id.otp);
        verifiyBtn = v.findViewById(R.id.verifyBtn);

        verifiyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!otp.getText().toString().equals("")){

                    progressBar.setVisibility(View.VISIBLE);
                    number = otp.getText().toString();
                    sendVerificationCode(number);
                    otp.setHint("Code");


                }
                else {
                    otp.setError("");
                }
            }
        });

        return v;

    }
    private void sendVerificationCode(String no) {
        this.number = no;
        if(no.charAt(0) == '0')
            number = "+49"+no.substring(1);
        if(no.charAt(0)=='1')
            number = "+49" + no;


        PhoneAuthProvider.getInstance().verifyPhoneNumber(


                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            progressBar.setVisibility(View.GONE);
            if (code != null) {
                otp.setText(code);
                verifiyBtn.setText("verify code");
                //verifying the code
                verifyVerificationCode(code);

            }



        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
            verifiyBtn.setText("verify code");
            verifiyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setCode();
                }
            });
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            progressBar.setVisibility(View.GONE);
                            getActivity().getSupportFragmentManager().beginTransaction().remove(PhoneVerification.this).commit();

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";
                            Toast.makeText(getContext(),"Verification Failed",Toast.LENGTH_SHORT);
                            Log.e(TAG, "onComplete: Failed=" + task.getException().getMessage());

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";

                            }


                        }
                    }
                });
    }


  private void setCode(){

        verifiyBtn.setText("verify code");
        String code = otp.getText().toString();
        if(!code.equals("")) {
            verifyVerificationCode(code);
        }
        else {
            otp.setError("wrong code");
        }



    }
}
