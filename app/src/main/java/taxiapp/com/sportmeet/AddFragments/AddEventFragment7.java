package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gc.materialdesign.views.Slider;

import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment7 extends Fragment {



    private TextView title;
    private TextView category;
    private TextView location;
    public TextView selectedDate;
    private TextView timeSelectedView;
    private TextView description;
    private TextView participant;
    private AddEventFrag aF;




    public AddEventFragment7(AddEventFrag aF){

        this.aF = aF;

    }


    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout7, container, false);

        selectedDate = v.findViewById(R.id.whenTextView2);
        title = v.findViewById(R.id.titleTextView);
        category = v.findViewById(R.id.categoryTextView);
        location = v.findViewById(R.id.whereTextView);
        timeSelectedView = v.findViewById(R.id.whatTimeTextView);
        description = v.findViewById(R.id.descriptionTextView);

        selectedDate.setText(aF.eventDetails.get("date"));
        title.setText(aF.eventDetails.get("name"));
        category.setText((aF.eventDetails.get("category")));
        location.setText(aF.eventDetails.get("location"));
        timeSelectedView.setText(aF.eventDetails.get("time"));
        description.setText(aF.eventDetails.get("description"));

        participant = v.findViewById(R.id.participantsText);




        return v;
    }
}
