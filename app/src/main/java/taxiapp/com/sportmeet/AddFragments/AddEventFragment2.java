package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;


import com.gc.materialdesign.views.Slider;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import taxiapp.com.sportmeet.CustomViewAdapterCategory;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment2 extends Fragment {
    private Spinner dropdown;

    private GeoDataClient mGeoDataClient;
    public  String woT ="";
    private LatLng latLng;
    private PlaceAutocompleteFragment autocompleteFragment1;
    private int numberParticipants = 0;
    private com.gc.materialdesign.views.Slider numberPicker;
    private AddEventFrag aF;
    private TextView participantsTxt;
    private String city;
    private  Geocoder mGeocoder;

    @SuppressLint("ValidFragment")
    public AddEventFragment2(AddEventFrag aF){

        this.aF = aF;



    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout2, container, false);


        participantsTxt = v.findViewById(R.id.currentParticipants);

        numberPicker = v.findViewById(R.id.slider);

        numberPicker.setOnValueChangedListener(new Slider.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {

                participantsTxt.setText(String.valueOf(value));
                numberParticipants = value;
            }
        });

       mGeocoder = new Geocoder(getActivity(), Locale.getDefault());

        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(getActivity(), null);

        // Construct a PlaceDetectionClient.
        PlaceDetectionClient mPlaceDetectionClient = Places.getPlaceDetectionClient(getActivity(), null);


        autocompleteFragment1   = (PlaceAutocompleteFragment)getActivity()
                .getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment1.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            private static final String TAG = "places";

            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                woT = (String) place.getName();
                latLng = place.getLatLng();
                try {
                    city = getCityNameByCoordinates(place.getLatLng().latitude,place.getLatLng().longitude);

                }catch (Exception e){
                    System.out.println("couldn't find City");
                }

            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });




        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            if(woT.length()>0&&numberParticipants>0){


                aF.eventDetails.put("numberParticipants", String.valueOf(numberParticipants));
                aF.eventDetails.put("location", woT);
                aF.eventDetails.put("latitude", String.valueOf(latLng.latitude));
                aF.eventDetails.put("longitude", String.valueOf(latLng.longitude));
                aF.eventDetails.put("city", city);
                aF.switchLayout(8);

                aF.addEvent();
                aF.addEventFinished();
            }

            }
        });


        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(autocompleteFragment1 != null && getActivity() != null && !getActivity().isFinishing()) {
            getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment1).commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
    private String getCityNameByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            return addresses.get(0).getLocality();
        }
        return null;
    }
}
