package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stripe.android.model.Card;
import com.stripe.android.view.CardInputWidget;

import org.xml.sax.ErrorHandler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment8 extends Fragment {

    private TextView title;
    private TextView category;
    private TextView location;
    public TextView selectedDate;
    private TextView timeSelectedView;
    private TextView description;
    private AddEventFrag aF;
    private TextView participants;

    public AddEventFragment8(AddEventFrag aF){

        this.aF = aF;

    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout8, container, false);


        selectedDate = v.findViewById(R.id.whenTextView2);
        title = v.findViewById(R.id.titleTextView);
        category = v.findViewById(R.id.categoryTextView);
        location = v.findViewById(R.id.whereTextView);
        timeSelectedView = v.findViewById(R.id.whatTimeTextView);
        description = v.findViewById(R.id.description);
        participants = v.findViewById(R.id.participantsTextView);

        participants.setText(aF.eventDetails.get("numberParticipants"));
        selectedDate.setText(aF.eventDetails.get("date"));
        title.setText(aF.eventDetails.get("name"));
        category.setText((aF.eventDetails.get("category")));
        location.setText(aF.eventDetails.get("location"));
        timeSelectedView.setText(aF.eventDetails.get("time"));
        description.setText(aF.eventDetails.get("description"));

        CardInputWidget mCardInputWidget = (CardInputWidget) v.findViewById(R.id.card_input_widget);

        Card cardToSave = mCardInputWidget.getCard();
        if (cardToSave == null) {

        }






        aF.weiter.setText("CREATE");

        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





            }
        });






        return v;
    }



}
