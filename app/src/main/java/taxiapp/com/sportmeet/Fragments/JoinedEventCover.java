package taxiapp.com.sportmeet.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.sql.Date;

import taxiapp.com.sportmeet.Chat.ChatEventFragment;
import taxiapp.com.sportmeet.EditEvent.EditEventBack;
import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;

import static android.content.Context.MODE_PRIVATE;


public class JoinedEventCover extends Fragment implements View.OnClickListener {
    private Event event;
    private String userID;
    private static final String DESCRIBABLE_KEY = "userID";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.joined_event_fragment, container, false);
        userID =  getArguments().getString("userID");
        event = (Event) getArguments().getSerializable("event");

        TextView description = v.findViewById(R.id.eventDescription);
        TextView ortText = v.findViewById(R.id.eventLocation);
        TextView  eventName = v.findViewById(R.id.eventName);
        TextView participants = v.findViewById(R.id.participants);
        TextView startEndTIme = v.findViewById(R.id.startEndTIme);
        TextView dayWritten = v.findViewById(R.id.dayWritten);
        TextView monthWritten = v.findViewById(R.id.monthWrtitten);
        TextView dayNumber = v.findViewById(R.id.dayNumber);
        TextView category = v.findViewById(R.id.eventCategory);

        participants.setText("Participants:"+" "+event.getEventMaxTeilnehmer());
        startEndTIme.setText(event.getStartTime()+ " - "+ event.getEndTime());
        ortText.setText(event.getEventLocation());
        eventName.setText(event.getEventName());
        description.setText(event.getEventBeschreibung());

        category.setText(event.getEventKategorie());

        participants.setText("Participants:"+" "+event.getEventMaxTeilnehmer());
        startEndTIme.setText(event.getStartTime()+ " - "+ event.getEndTime());
        ortText.setText(event.getEventLocation());
        eventName.setText(event.getEventName());
        description.setText(event.getEventBeschreibung());

        Date date = new Date(Integer.valueOf(event.date.year),
                Integer.valueOf(event.date.month)-1,Integer.valueOf(event.date.day)-1);



        String dayWrittenString = (String) android.text.format.DateFormat.format("EEEE", date);

        String monthWrittenString = (String) android.text.format.DateFormat.format("MMMM", date);

        dayWritten.setText(dayWrittenString);
        dayNumber.setText(String.valueOf(event.date.day));
        monthWritten.setText(monthWrittenString);


        ImageView edit = v.findViewById(R.id.editIcon);
        TextView editTextView = v.findViewById(R.id.editTextView);

        if(userID.equals(event.eventCreator)){

            editTextView.setVisibility(View.VISIBLE);
            edit.setVisibility(View.VISIBLE);

            editTextView.setOnClickListener(this);
            edit.setOnClickListener(this);



        }


        Button chatButton = v.findViewById(R.id.chatButton);
        chatButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                Fragment jointFragment = new ChatEventFragment();
                Bundle b = new Bundle();
                b.putString("userID", userID);
                SharedPreferences prefs = getActivity().getSharedPreferences("user", MODE_PRIVATE);
                String username = prefs.getString("username",
                        "FAIL");
                b.putString("username",username);
                b.putSerializable("event", event);
                jointFragment.setArguments(b);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.frameLayoutAdd,jointFragment,"event").addToBackStack("1");
                fragmentTransaction.commit();

            }
        });

        return v;
    }

    @Override
    public void onClick(View v) {
        //preventing doubleTab
        Fragment test = getFragmentManager().findFragmentByTag("editfrag");

        if (test != null && test.isVisible()) {
            return;
        } else {
            Fragment editFragment = new EditEventBack(event);
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.frameLayoutAdd, editFragment, "editfrag").addToBackStack("1");
            fragmentTransaction.commit();
        }
    }



}
