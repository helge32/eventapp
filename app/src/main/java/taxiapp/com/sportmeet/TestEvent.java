package taxiapp.com.sportmeet;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

import taxiapp.com.sportmeet.EventTiming;

public class TestEvent {

    @SerializedName("eventID")
    public String eventID;
    @SerializedName("eventCreator")
    public String eventCreator;
    @SerializedName("eventName")
    public String eventName;
    @SerializedName("eventKategorie")
    public String eventKategorie;
    @SerializedName("eventDate")
    public String eventDate;
    @SerializedName("eventMaxTeilnehmer")
    public int eventMaxTeilnehmer;
    @SerializedName("eventLocation")
    public String eventLocation;
    @SerializedName("latitude")
    public double latitude;
    @SerializedName("longitude")
    public double longitude;
    @SerializedName("eventBeschreibung")
    public String eventBeschreibung;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;

    public TestEvent(String eventID, String eventCreator, String eventName, String eventKategorie, String eventDate, int eventMaxTeilnehmer, String eventLocation, double latitude, double longitude, String eventBeschreibung, String startTime, String endTime) {
        this.eventID = eventID;
        this.eventCreator = eventCreator;
        this.eventName = eventName;
        this.eventKategorie = eventKategorie;
        this.eventDate = eventDate;
        this.eventMaxTeilnehmer = eventMaxTeilnehmer;
        this.eventLocation = eventLocation;
        this.latitude = latitude;
        this.longitude = longitude;
        this.eventBeschreibung = eventBeschreibung;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getEventID() {
        return eventID;
    }

    public String getEventName() {
        return eventName;
    }

    public String getEventKategorie() {
        return eventKategorie;
    }

    public String getEventDate() {
        return eventDate;
    }

    public int getEventMaxTeilnehmer() {
        return eventMaxTeilnehmer;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getEventBeschreibung() {
        return eventBeschreibung;
    }

    public void setEventID(String eventID) {
        this.eventID = eventID;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setEventKategorie(String eventKategorie) {
        this.eventKategorie = eventKategorie;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public void setEventMaxTeilnehmer(int eventMaxTeilnehmer) {
        this.eventMaxTeilnehmer = eventMaxTeilnehmer;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setEventBeschreibung(String eventBeschreibung) {
        this.eventBeschreibung = eventBeschreibung;
    }
}
