package taxiapp.com.sportmeet.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;

import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.lang.ref.WeakReference;
import java.sql.Date;
import java.text.DateFormat;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.Server.RetrofitI;
import taxiapp.com.sportmeet.Server.ServerKlasse;
import taxiapp.com.sportmeet.User;


@SuppressLint("ValidFragment")
public class EventFragment extends Fragment implements OnMapReadyCallback {



    private Button teilnehmenButton;


    private Event event;
    private String userID;
    private RelativeLayout relativeLayout;
    @SuppressLint("ValidFragment")


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.event_info_fragment, container, false);

        userID =  getArguments().getString("userID");
        event = (Event) getArguments().getSerializable("event");



        relativeLayout = v.findViewById(R.id.back);
        TextView description = v.findViewById(R.id.eventDescription);
        TextView ortText = v.findViewById(R.id.eventLocation);
        TextView  eventName = v.findViewById(R.id.eventName);
        TextView participants = v.findViewById(R.id.participants);
        TextView startEndTIme = v.findViewById(R.id.startEndTIme);
        TextView dayWritten = v.findViewById(R.id.dayWritten);
        TextView monthWritten = v.findViewById(R.id.monthWrtitten);
        TextView dayNumber = v.findViewById(R.id.dayNumber);
        TextView category = v.findViewById(R.id.eventCategory);
        description.setMovementMethod(new ScrollingMovementMethod());
        category.setText(event.getEventKategorie());

        participants.setText("Participants:"+" "+event.getEventMaxTeilnehmer());
        startEndTIme.setText(event.getStartTime()+ " - "+ event.getEndTime());
        ortText.setText(event.getCity());
        eventName.setText(event.getEventName());
        description.setText(event.getEventBeschreibung());

        Date date = new Date(Integer.valueOf(event.date.year),
                Integer.valueOf(event.date.month)-1,Integer.valueOf(event.date.day)-1);



        String dayWrittenString = (String) android.text.format.DateFormat.format("EEEE", date);

        String monthWrittenString = (String) android.text.format.DateFormat.format("MMMM", date);

        dayWritten.setText(dayWrittenString);
        dayNumber.setText(String.valueOf(event.date.day));
        monthWritten.setText(monthWrittenString);




        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng eventCity = new LatLng(event.latitude, event.longitude);
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(eventCity,10));
                CircleOptions  circleOptions = new CircleOptions();
                googleMap.addCircle( circleOptions.center(eventCity));
                circleOptions.radius(3500);
                circleOptions.strokeColor(ContextCompat.getColor(getContext(), R.color.colorAccent));
                circleOptions.strokeWidth(2);
                circleOptions.fillColor(ContextCompat.getColor(getContext(), R.color.colorAccentMap));
                googleMap.addCircle(circleOptions);
            }
        });



        teilnehmenButton = v.findViewById(R.id.buttonTei);



        teilnehmenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /**
                 *
                 * Daten speichern
                 */


                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {

                    joinEvent();

                } else {

                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    PhoneVerification phoneVerification = new PhoneVerification();
                    transaction.add(R.id.verificationContainer, phoneVerification).addToBackStack("1").commit();





                }
            }
        });

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

              //  fragmentTransaction = getFragmentManager().beginTransaction();
              //  fragmentTransaction.remove(f.eventFragment).commit();

                return true;
            }
        });

        return v;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {


    }


    private void joinEvent(){
        Retrofit  retrofit = new Retrofit.Builder().baseUrl(RetrofitI.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RetrofitI retrofitI = retrofit.create(RetrofitI.class);


        String id = event.getEventID();
        Call<Void> call = retrofitI.joinEvent("joinEvent",userID,event.getEventID());

        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                EventBus eventBus = EventBus.getDefault();
                eventBus.post(new JoinEventMessages());

                getActivity().onBackPressed();

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });



    }




}
