package taxiapp.com.sportmeet.Welcome;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;

import taxiapp.com.sportmeet.R;

public class WelcomeFragment1 extends Fragment {
    private TextInputEditText username;
    private TextInputEditText otp;
    public ProgressBar progressBar;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.welcome_fragment1, container, false);
        username = v.findViewById(R.id.usernameEditText);
        otp = v.findViewById(R.id.otp);
        progressBar = v.findViewById(R.id.progress);

        return v;
    }





    public String getFromUser() {

        return username.getText().toString();
    }
    public void setError(){
        username.setError("");
    }






}
