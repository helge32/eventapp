package taxiapp.com.sportmeet.AddFragments;


import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;

import java.util.Calendar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment4 extends Fragment {
    public String date ="";
    private CalendarView datePicker;
    private AddEventFrag aF;
    private TextView title;
    private TextView category;
    private TextView location;
    public TextView selectedDate;



    public AddEventFragment4(AddEventFrag aF){

        this.aF = aF;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout4, container, false);

        datePicker = v.findViewById(R.id.datePicker);

        selectedDate = v.findViewById(R.id.whenTextView2);
        title = v.findViewById(R.id.titleTextView);
        category = v.findViewById(R.id.categoryTextView);
        location = v.findViewById(R.id.whereTextView);

        selectedDate.setText(date);
        title.setText(aF.eventDetails.get("name"));
        category.setText((aF.eventDetails.get("category")));
        location.setText(aF.eventDetails.get("location"));


        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!date.equals("")){

                    aF.eventDetails.put("date",date);
                    aF.switchLayout(5);
                }

            }
        });


datePicker.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
    @Override
    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

        final Calendar c = Calendar.getInstance();




        date = year+ "-"+month+1 +"-"+dayOfMonth;
        date = date;
        System.out.println(date);
        selectedDate.setText(date);

    }

});
        return v;
    }





}
