package taxiapp.com.sportmeet;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.annotation.NonNull;


import taxiapp.com.sportmeet.Welcome.WelcomeActivity;

import android.telephony.TelephonyManager;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class RegisterTools extends AppCompatActivity implements UserRegistrationHelper {
    private static final int REQUEST_READ_PHONE_STATE = 1;
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

    @Override
    public String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Override
    public String sha1Hash(String toHash) {
        String hash = null;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] bytes = toHash.getBytes(StandardCharsets.UTF_8);
            digest.update(bytes, 0, bytes.length);
            bytes = digest.digest();

            hash = bytesToHex(bytes);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hash;
    }

    @Override
    public String getNewUserKey() {

        String uniqueID = UUID.randomUUID().toString();
        return  uniqueID;

            /*
            Datenbankabfrage ob User schon vorhanden ist
             */
    }

    @Override
    public String checkUser() {

        SharedPreferences prefs = getSharedPreferences("user",
                MODE_PRIVATE);
        String key = prefs.getString("key",
                "FAIL");
        return key;
    }

    @Override
    public String getNumber() {

        SharedPreferences prefs = getSharedPreferences("user",
                MODE_PRIVATE);
        String key = prefs.getString("number",
                "FAIL");
        return key;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {



                }
            }
            if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                    Intent restart = new Intent(RegisterTools.this, WelcomeActivity.class);
                    startActivity(restart);
                    finish();
                }
            }
        }
    }
}
