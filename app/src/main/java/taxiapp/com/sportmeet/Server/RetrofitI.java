package taxiapp.com.sportmeet.Server;

import com.google.gson.JsonElement;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.User;

public interface RetrofitI {

    String baseUrl = "https://eventmeetapp.com/";

    /*

    start.php Requests

     */
    @FormUrlEncoded
    @POST("start.php/")
    Call<ArrayList<Event>> getEvents(@Field("event_db_request_type") String requestType,
                                     @Field("user_key") String userKey);
    @FormUrlEncoded
    @POST("start.php/")
    Call<Void> addEvent(@Field("event_db_request_type") String requestType,
                        @Field("event_date") String date,
                        @Field("event_maxTeilnehmer") String maxParticipants,
                        @Field("event_beschreibung") String description,
                        @Field("event_kategorie") String category,
                        @Field("event_name") String name,
                        @Field("event_location") String location,
                        @Field("event_latitude") String latitude,
                        @Field("event_longitude") String longitude,
                        @Field("event_startTime") String startTime,
                        @Field("event_endTime") String endTime,
                        @Field("event_creator") String creator,
                        @Field("event_city") String city);

    @FormUrlEncoded
    @POST("start.php/")
    Call<Void> joinEvent(@Field("event_db_request_type") String requestType,
                         @Field("user_key") String userKey,
                         @Field("event_id") String eventId);

    @FormUrlEncoded
    @POST("start.php/")
    Call<ArrayList<Event>> loadMyEvents(@Field("event_db_request_type") String requestType,
                                        @Field("user_key") String userKey);
/*

    user.php Requests

 */


    @FormUrlEncoded
    @POST("user.php/")
    Call<JsonElement> checkUser(@Field("event_db_request_type") String requestType,
                                @Field("user_key") String userKey);

    @FormUrlEncoded
    @POST("user.php/")
    Call<ResponseBody> checkUser2(@Field("event_db_request_type") String requestType,
                                  @Field("user_key") String userKey);


    @FormUrlEncoded
    @POST("user.php/")
    Call<ArrayList<User>> updateProfile(@Field("event_db_request_type") String requestType,
                                        @Field("user_key") String userKey);
    @FormUrlEncoded
    @POST("user.php/")
    Call<Void> createUser(@Field("event_db_request_type") String requestType,
                          @Field("event_userValue") String userKey,
                          @Field("event_name") String name);



}
