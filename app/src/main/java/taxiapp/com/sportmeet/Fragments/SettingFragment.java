package taxiapp.com.sportmeet.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;

import static android.content.Context.MODE_PRIVATE;

public class SettingFragment extends Fragment {

    private String userName;
    private TextView usernameTxt;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.setting_fragment, container, false);
        SharedPreferences prefs = getActivity().getSharedPreferences("user", MODE_PRIVATE);
            userName = prefs.getString("username", "FAIL");
       usernameTxt = v.findViewById(R.id.username);

       usernameTxt.setText("Hi " + userName + " !");



        return  v;


    }

}
