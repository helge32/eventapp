package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment5 extends Fragment {

    private AddEventFrag aF;
    private TextView title;
    private TextView category;
    private TextView location;
    public TextView selectedDate;
    private TimePicker timePicker;

    private String time;

    @SuppressLint("ValidFragment")
    public AddEventFragment5(AddEventFrag aF){

        this.aF = aF;


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.addevent_fragment_layout5, container, false);

        timePicker =  v.findViewById(R.id.timePicekr);
        selectedDate = v.findViewById(R.id.whenTextView2);
        title = v.findViewById(R.id.titleTextView);
        category = v.findViewById(R.id.categoryTextView);
        location = v.findViewById(R.id.whereTextView);




        selectedDate.setText(aF.eventDetails.get("date"));
        title.setText(aF.eventDetails.get("name"));
        category.setText((aF.eventDetails.get("category")));
        location.setText(aF.eventDetails.get("location"));

        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {

                time = hourOfDay+":"+minute;



            }
        });


        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            aF.eventDetails.put("time",time);
            aF.switchLayout(6);



            }
        });

            return v;
        }

    }


