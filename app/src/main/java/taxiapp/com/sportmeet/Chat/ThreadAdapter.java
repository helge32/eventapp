package taxiapp.com.sportmeet.Chat;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import taxiapp.com.sportmeet.R;

/**
 * Created by Belal on 5/29/2016.
 */
//Class extending RecyclerviewAdapter
public class ThreadAdapter extends RecyclerView.Adapter<ThreadAdapter.ViewHolder> {
private String id;
  private String previousMessage ="";

    //user id
    private int userId;
    private Context context;

    //Tag for tracking self message
    private int SELF = 1;

    //ArrayList of messages object containing all the messages in the thread
    private ArrayList<Message> messages;

    //Constructor
    public ThreadAdapter(Context context, ArrayList<Message> messages, String userId){
        this.id = userId;
        this.messages = messages;
        this.context = context;
    }

    //IN this method we are tracking the self message
    @Override
    public int getItemViewType(int position) {
        try {
            //getting message object of current position
            Message message = messages.get(position);

            String a = message.getUsersId();
            //If its owner  id is  equals to the logged in user id
            if (message.getUsersId().equals(id)) {
                //Returning self
                return SELF;
            }
        }catch (Exception e){}
        //else returning position
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

      try {
          //Creating view
          View itemView;
          //if view type is self
          if (viewType == SELF) {
              //Inflating the layout self
              itemView = LayoutInflater.from(parent.getContext())
                      .inflate(R.layout.chat_item_self, parent, false);
          } else {
              //else inflating the layout others
              itemView = LayoutInflater.from(parent.getContext())
                      .inflate(R.layout.chat_item_other, parent, false);
          }
          return new ViewHolder(itemView);
      }catch (Exception e){}
        //returing the view
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

           Message message = messages.get(position);
           holder.textViewMessage.setText(message.getMessage());


           String time = message.getSentAt();
           holder.textViewTime.setText(message.getUserName() + ", " + time);





    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    //Initializing views
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewMessage;
        public TextView textViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewMessage = itemView.findViewById(R.id.textViewMessage);
            textViewTime = itemView.findViewById(R.id.textViewTime);
        }
    }


}