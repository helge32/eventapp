package taxiapp.com.sportmeet;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

@SuppressLint("ValidFragment")
public class PermissionDialog extends DialogFragment {
    private  MainActivity mainActivity;
    public PermissionDialog(MainActivity mainActivity){

        this.mainActivity = mainActivity;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Bitte Permission geben")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        mainActivity.checkUser();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    mainActivity.checkUser();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}