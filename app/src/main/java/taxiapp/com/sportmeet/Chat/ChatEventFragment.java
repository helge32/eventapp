package taxiapp.com.sportmeet.Chat;


import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import taxiapp.com.sportmeet.Event;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;
import taxiapp.com.sportmeet.User;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class ChatEventFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;


    public ArrayList<Message> messagesBuffer = new ArrayList<>();
    public ArrayList<Message> messages = new ArrayList<>();
    private int curretnSize = -1;
    private Event event;
    private String currentUser = "";

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private TextView eventNameTxt;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm a");
    private String userID;
    private String userName;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.myevent_chat_fragment, container, false);

        userID = getArguments().getString("userID");
        userName = getArguments().getString("username");
        event = (Event) getArguments().getSerializable("event");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        eventNameTxt = v.findViewById(R.id.eventName);
        eventNameTxt.setText(event.getEventName());
        currentUser = userName;


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        refreshMessages();

        new CountDownTimer(9000000, 10000) {
            public void onTick(long millisUntilFinished) {
                refreshMessages();
            }

            public void onFinish() {

            }
        }.start();

        layoutManager = new LinearLayoutManager(getContext());
        final String userId = userID;
        recyclerView = v.findViewById(R.id.recyclerView);
        adapter = new ThreadAdapter(recyclerView.getContext(), messagesBuffer, userId);
        recyclerView.setLayoutManager(layoutManager);




        recyclerView.scrollToPosition(adapter.getItemCount() - 1);

        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override

            public void onLayoutChange(View v, int left, int top, int right,int bottom, int oldLeft, int oldTop,int oldRight, int oldBottom)
            {

                recyclerView.scrollToPosition(adapter.getItemCount() -1);

            }
        });


        final EditText messageEditText = v.findViewById(R.id.messageEditText);
        ImageView sendButton = v.findViewById(R.id.imageView_send);


        //Sending Message
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(messageEditText.getText().toString().equals(""))) {
                    String message = messageEditText.getText().toString();

                    Map<String, Object> messageValues = new HashMap<>();
                    messageValues.put("message", message);
                    messageValues.put("sender", userId);
                    messageValues.put("username", userName);
                    messageValues.put("timestamp",  FieldValue.serverTimestamp());

                    db.collection(event.getEventID())
                            .add(messageValues)
                            .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                @Override
                                public void onSuccess(DocumentReference documentReference) {
                                 refreshMessages();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.w(TAG, "Error adding document", e);
                                }
                            });
                    Message message1 = new Message(userId, message, "", userName);
                    messagesBuffer.add(message1);

                    recyclerView.getRecycledViewPool().clear();
                    refreshMessages();
                    messageEditText.setText("");

                }
            }
        });


        return v;

    }

    private void scrollToBottom() {


        if (messagesBuffer.size() != curretnSize) {

            curretnSize = messagesBuffer.size();
            recyclerView.swapAdapter(adapter, true);
            adapter.notifyDataSetChanged();
            recyclerView.scrollToPosition(adapter.getItemCount() - 1);
        }
    }

    private void refreshMessages() {

        db.collection(event.getEventID())
                .orderBy("timestamp", Query.Direction.ASCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            try{

                                messagesBuffer.clear();
                                for (QueryDocumentSnapshot document : task.getResult()) {

                                    Date time = document.getTimestamp("timestamp").toDate();
                                    simpleDateFormat.setTimeZone(TimeZone.getDefault());

                                    Message messagObject = new Message(document.getString("sender"), document.getString("message"),
                                            simpleDateFormat.format(time.getTime()),document.getString("username"));
                                    messagesBuffer.add(messagObject);
                                    scrollToBottom();
                                }
                            }catch (Exception e){


                            }

                        } else {
                            Log.w(TAG, "Error getting Mesages.", task.getException());
                        }
                    }
                });

    }



}