package taxiapp.com.sportmeet.AddFragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceDetectionClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import taxiapp.com.sportmeet.MainActivity;
import taxiapp.com.sportmeet.R;

@SuppressLint("ValidFragment")
public class AddEventFragment3 extends Fragment {


    private PlaceAutocompleteFragment autocompleteFragment1;



    private GeoDataClient mGeoDataClient;
    public  String woT ="";
   private LatLng latLng;
    private AddEventFrag aF;
   private View   v;
   private TextView title;
    private TextView category;




    public AddEventFragment3(AddEventFrag aF){

       this.aF = aF;

    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.addevent_fragment_layout3, container, false);

        title = v.findViewById(R.id.titleTextView);
        category = v.findViewById(R.id.categoryTextView);

        title.setText(aF.eventDetails.get("name"));
        category.setText((aF.eventDetails.get("category")));


        // Construct a GeoDataClient.
        mGeoDataClient = Places.getGeoDataClient(getActivity(), null);

        // Construct a PlaceDetectionClient.
        PlaceDetectionClient mPlaceDetectionClient = Places.getPlaceDetectionClient(getActivity(), null);


       autocompleteFragment1   = (PlaceAutocompleteFragment)getActivity()
                .getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment1.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            private static final String TAG = "places";

            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i(TAG, "Place: " + place.getName());
                woT = (String) place.getName();
                latLng = place.getLatLng();


            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i(TAG, "An error occurred: " + status);
            }
        });

        aF.weiter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



             if (!woT.equals("")) {



                        aF.eventDetails.put("location",woT);
                        aF.eventDetails.put("latitude",String.valueOf(latLng.latitude));
                        aF.eventDetails.put("longitude",String.valueOf(latLng.longitude));

                        aF.switchLayout(4);




                    }

                }

        });





        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if(autocompleteFragment1 != null && getActivity() != null && !getActivity().isFinishing()) {
            getActivity().getFragmentManager().beginTransaction().remove(autocompleteFragment1).commit();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }



}
